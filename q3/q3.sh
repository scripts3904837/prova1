#!/bin/bash

# Criação de variáveis 

c="João Pessoa" #Variável de atribuição direta,considera tudo que está dentro das aspas
cs=150 #Permite substituição
nm='Ramon' #Não permite substituição

#Pedindo ao usuário
read -p "Digite sua idade" id

#Variáveis de parâmetro de linha de comando

x=$1
y=$2

#Variáveis automática são variáveis predefinidas que fornenem informações sobre o ambiente e a execução do script.são chamadas automáticas porque o sistema as cria automaticamente e as mantém atualizadas conforme o script é executado.
#Notamos que existe a maneira de criar variáveis e atribuir valores diretamente a ela. Mas também temos a possibilidade de pedir para que o usuário atribuia o valor, tornando o script mais interativo e flexível, pois ele pode ser adaptado de acordo com a entrada do usuário.
