#!/bin/bash

read -p "Primeiro diretório: " d1
read -p "Segundo diretório: " d2
read -p "Terceiro diretório: " d3

dx=$(find $d1 -type f -name '*.xls' | grep -c '.xls')
db=$(find $d1 -type f -name '*.bmp' | grep -c '.bmp')
dd=$(find $d1 -type f -name '*.docx' | grep -c '.docx')

bx=$(find $d2 -type f -name '*.xls' | grep -c '.xls')
bb=$(find $d2 -type f -name '*.bmp' | grep -c '.bmp')
bd=$(find $d2 -type f -name '*.docx' | grep -c '.docx')

cx=$(find $d3 -type f -name '*.xls' | grep -c '.xls')
cb=$(find $d3 -type f -name '*.bmp' | grep -c '.bmp')
c=$(find $d3 -type f -name '*.docx' | grep -c '.docx')

echo "No diretório $d1, há $dx arquivos .xls, $db arquivos .bmp e $dd arquivos .docx."
echo "No diretório $d2, há $bx arquivos .xls, $bb arquivos .bmp e $bd arquivos .docx."
echo "No diretório $d3, há $cx arquivos .xls, $cb arquivos .bmp e $c arquivos .docx."

