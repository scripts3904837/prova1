#!/bin/bash


echo " Seja muito bem-vindo!"

echo " Selecione uma das seguintes opções para continuar:"
echo " 1. Criar um novo arquivo .zip"
echo " 2. Escolher um arquivo .zip existente" 
read opcao

if [ $opcao -eq 1 ]
then 
	echo " Informe o novo arquivo .zip: "
	read novo_arquivo
	echo " Informe o novo arquivo ou diretório que deseja compactar: "
	read arquivo
	zip $novo_arquivo $arquivo

elif [ $opcao -eq 2 ]
then	
	echo " Informe o nome do arquivo .zip existente " 
	read arquivo_existente
	echo " Selecione uma opção: "
	echo "1. Listar conteúdo do arquivo .zip"
	echo "2. Pré-visualizar um arquivo"
	echo "3. Adicionar um novo arquivo ao .zip"
	echo "4. Remover um arquivo do .zip"
	echo "5. Extrair todo o conteúdo do .zip "
	echo "6. Extrair um arquivo específico do .zip"
	read opcao_zip

	if [ $opcao_zip -eq 1 ]; then
		unzip -l $arquivo_existente

	elif [ $opcao_zip -eq 2 ]; then
		echo "Informe o nome do arquivo que deseja pré-visualizar: "
		read arquivo_visualizar
		unzip -p $arquivo_existente $arquivo_visualizar

	elif [ $opcao_zip -eq 3 ]; then
		echo "Informe o nome do arquivo que deseja adicionar: "
		read arquivo_adicionar

		zip $arquivo_existente $arquivo_adicionar
		echo "$arquivo_adicionar adicionado com sucesso!"i
		
	elif [ $opcao_zip -eq 4 ]; then
		echo "Informe o arquivo que deseja remover: "
		read arquivo_remover
		zip -d $arquivo_existente $arquivo_remover

	elif [ $opcao_zip -eq 5 ]; then
		unizp $arquivo_existente
		echo "Conteúdo Extraído com sucesso!"

	elif [ $opcao_zip -eq 6 ]; then
		echo "Digite o nome do arquivo que deseja extrair: "
		read arquivo_extrair
		unzip $arquivo_existente $arquivo_extrair
		echo "$arquivo_extrair extraído com sucesso!"


	else
		echo "Opção Inváida."
	fi
else
	echo "Opção Inválida."
	
fi

