#!/bin/bash 


echo "Informações sobre Dispositivos USB: "
lsusb

echo "Informações sobre Dispositivos PCI: "
lspci

echo "Informações sobre o Hardware: "
lshw -short

echo "Informações sobre a CPU: "
lscpu
